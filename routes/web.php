<?php

use GuzzleHttp\Client;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', function () {
    return view('welcome');
});


Route::get('users/{id?}', function($id = null) {
    if ($id == null) {
        return 'No hay id de usuario';
    } else {
        return 'Usuario con id: ' . $id;
    }
})->where('id', '[0-9]+');


Route::get('listPersonas', function() {
    $client = new Client([
        // Base URI is used with relative requests
        'base_uri' => config('global.urlMainAPI'),
        // You can set any number of default request options.
        'timeout'  => 2.0,
    ]);

    $response = $client->request('GET', 'listPersonas');
    $listPersonas = json_decode($response->getBody()->getContents());
    return view('listPersonas', ['listPersonas' => $listPersonas]);
});

Route::get('addPersona', function() {
    return view('addPersona');
});

Route::get('updatePersona/{id}', function($id) {
    $client = new Client([
        'base_uri' => config('global.urlMainAPI'),
        'timeout' => 2.0
    ]);

    $response = $client->request('GET', 'getPersonaById/'.$id);
    $persona = json_decode($response->getBody()->getContents());
    return view('updatePersona', ['persona' => $persona]);
});

