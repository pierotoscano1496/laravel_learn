@extends('templatePersonas')

@section('body')
    <h1>Actualizar datos de persona</h1>
    <br>
    <form onsubmit="return updatePersona()">
        <input type="hidden" value="{{ $persona->_id }}">
        <div class="form-group">
            <label for="nombres">Nombres</label>
            <input id="nombres" type="text" name="nombres" class="form-control" value="{{ $persona->nombres }}">
        </div>
        <div class="form-group">
            <label for="apellidos">Apellidos</label>
            <input id="apellidos" type="text" name="apellidos" class="form-control" value="{{ $persona->apellidos }}">
        </div>
        <div class="form-group">
            <label for="apodo">Apodo</label>
            <input id="apodo" type="text" name="apodo" class="form-control" value="{{ $persona->apodo }}">
        </div>
        <div class="form-group">
            <label for="edad">Edad</label>
            <input id="edad" type="number" name="edad" class="form-control" value="{{ $persona->edad }}">
        </div>
        <button type="submit" class="btn btn-primary">Actualizar</button>
    </form>
@endsection

@section('script')
    <script>
        function updatePersona() {
            var nombres = document.getElementById('nombres').value;
            var apellidos = document.getElementById('apellidos').value;
            var apodo = document.getElementById('apodo').value;
            var edad = document.getElementById('edad').value;

            var formData = {
                nombres: nombres,
                apellidos: apellidos,
                apodo: apodo,
                edad: edad
            };

            fetch("{{ config('global.urlMainAPI').'updatePersona/'.$persona->_id }}", {
                headers : { "Content-Type" : "application/json; charset=UTF-8" },
                body : JSON.stringify(formData),
                method : "POST",
                mode: "cors"
            })
            .then(function(res) {
                console.log(res);
                if (res.ok) {
                    return res.text();
                } else {
                    alert('Error de registro');
                }
            })
            .then(function(text) {
                alert(text);
                window.location.href = getUrlMain() + 'listPersonas';
            })
            .catch(function(err) {
                document.getElementById('resMessage').innerHTML = err.message;
            });

            console.log(formData);
            return false;
        }
    </script>
@endsection