@extends('templatePersonas')

@section('body')
    <h2>Listado de personas</h1>
    <br>
    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nombres</th>
                <th scope="col">Apellidos</th>
                <th scope="col">Apodo</th>
                <th scope="col">Edad</th>
                <th scope="col">Opciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($listPersonas as $key => $persona)
                <tr>
                    <th scope="row">{{ $key + 1 }}</th>
                    <td>{{ $persona->nombres }}</td>
                    <td>{{ $persona->apellidos }}</td>
                    <td>{{ $persona->apodo }}</td>
                    <td>{{ $persona->edad }}</td>
                    <td>
                        <a href="/updatePersona/{{ $persona->_id }}" class="btn btn-info">Actualizar</a> | 
                        <button class="btn btn-success" data-id="{{ $persona->_id }}" onclick="deletePersona(this)">Eliminar</button>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <br>
    <a href="/addPersona" class="btn btn-success">Registrar persona</a>
@endsection

@section('script')
    <script>
        function deletePersona(button) {
            var confirmDelete = confirm('¿Desea eliminar a esta persona?');
            if (confirmDelete == true) {
                fetch("{{ config('global.urlMainAPI').'deletePersona/' }}" + button.getAttribute('data-id'), {
                    headers : { "Content-Type" : "application/json; charset=UTF-8" },
                    method : "GET",
                    mode: "cors"
                })
                .then(function(res) {
                    console.log(res);
                    if (res.ok) {
                        return res.text();
                    } else {
                        alert('Error al eliminar');
                    }
                })
                .then(function(text) {
                    alert(text);
                    location.reload();
                })
                .catch(function(err) {
                    alert(err.message);
                });
            }
        }
    </script>
    
@endsection
